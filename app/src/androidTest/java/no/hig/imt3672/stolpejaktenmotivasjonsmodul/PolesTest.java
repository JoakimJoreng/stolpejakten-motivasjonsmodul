package no.hig.imt3672.stolpejaktenmotivasjonsmodul;

import android.test.AndroidTestCase;

import com.google.android.gms.maps.model.LatLng;

public class PolesTest extends AndroidTestCase {

    public void test() throws Exception {
        String newname = "newname";
        int polecount = 80;

        Poles  poles = null;

        poles = Poles.getInstance();
        assertNotSame(null, poles);

        poles.initializePoles(getContext());

        while(poles.getList().size() < polecount){}
        assertNotSame(0, poles.getList().size());

        Pole pole = poles.getList().get(1);
        String oldname = pole.getName();
        pole.setName(newname);
        assertNotSame(oldname, newname);
        pole.setName(oldname);

        boolean[] difficultySettings;
        difficultySettings = new boolean[4];
        difficultySettings[0] = true;
        difficultySettings[1] = false;
        difficultySettings[2] = false;
        difficultySettings[3] = false;

        LatLng gjovikLatLng = new LatLng(60.79543,10.69163);
        Pole goalPole = poles.getRandomPoleByParameters(difficultySettings, 3000, gjovikLatLng);
        assertEquals(true, difficultySettings[goalPole.getDifficulty()-1]);
    }
}