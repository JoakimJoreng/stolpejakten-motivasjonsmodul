package no.hig.imt3672.stolpejaktenmotivasjonsmodul;

import android.test.AndroidTestCase;

import junit.framework.Assert;

/**
 * User Test Case
 * Created by Pål S on 10.12.2014.
 */
public class UserTest extends AndroidTestCase {

    private User user;

    public void testSetId(){
        user = new User();
        user.setId(5);
        Assert.assertTrue(user.getId() == 5);
    }

    public void testSetGuildName(){
        user = new User();
        user.setGuildName("name");
        Assert.assertEquals("name", user.getGuildName());
    }

    public void testIsLoggedIn(){
        user = new User();
        user.setLoggedIn(true);
        Assert.assertTrue(user.isLoggedIn());
    }

    public void testUsers(){
        user = new User();
        User user2 = new User();

        user.setId(5);
        user2.setId(6);

        Assert.assertFalse(user == user2);
    }

}
