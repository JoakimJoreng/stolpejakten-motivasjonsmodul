package no.hig.imt3672.stolpejaktenmotivasjonsmodul.challenge;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import java.util.ArrayList;
import java.util.List;

import no.hig.imt3672.stolpejaktenmotivasjonsmodul.R;
import no.hig.imt3672.stolpejaktenmotivasjonsmodul.challenge.hotandcold.HotAndColdListItem;

/**
 * Activity that display all the available challenges in a list
 */
@SuppressWarnings("EmptyMethod")
public class ChallengeList extends ListActivity {
    private List<ChallengeListItem> items;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.challenge_list);

        items = new ArrayList<ChallengeListItem>();
        items.add(new HotAndColdListItem());

        setListAdapter(new ChallengeListAdapter());
    }

    @Override
    public ListAdapter getListAdapter() {
        return super.getListAdapter();
    }

    private class ChallengeListAdapter extends ArrayAdapter<ChallengeListItem> {

        public ChallengeListAdapter() {

            super(ChallengeList.this, R.layout.challenge_list_item, items);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            if(convertView == null) {
                convertView = getLayoutInflater().inflate(R.layout.challenge_list_item, parent, false);
            }

            final ChallengeListItem currentItem = items.get(position);

            final Button startChallengeButton = (Button) convertView.findViewById(R.id.challenge_start);
            startChallengeButton.setText(currentItem.getName());

            startChallengeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent startChallenge =  currentItem.createChallengeIntent(v.getContext());
                    startActivity(startChallenge);
                }
            });

            ImageButton infoChallengeButton = (ImageButton) convertView.findViewById(R.id.challenge_about);
            infoChallengeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setMessage(currentItem.getDescription());
                    builder.setCancelable(true);
                    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });

                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
            });

            return convertView;
        }
    }
}
