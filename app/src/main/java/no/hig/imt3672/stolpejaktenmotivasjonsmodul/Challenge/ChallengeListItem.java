package no.hig.imt3672.stolpejaktenmotivasjonsmodul.challenge;

import android.content.Context;
import android.content.Intent;

/**
 * Abstract class for the presentation
 * "cards" that go into the ChallengeList
 */
@SuppressWarnings("SameParameterValue")
public abstract class ChallengeListItem {
    public final static int HOT_AND_COLD_ID = 1;
    private int id;
    private int name;
    private int description;

    public ChallengeListItem(int id, int name, int description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    /**
     * Sets up an Intent with relevant data
     * for the concrete implementations successor
     * activity.
     * @param context is the context from the
     * launching activity
     * @return intent
     */
    public abstract Intent createChallengeIntent(Context context);

    /**
     * Gets the id of the list item
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * Sets the id of the list item
     * @param id to be set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Gets the name of the list item
     * @return the name
     */
    public int getName() {
        return name;
    }

    /**
     * Sets the name of the list item
     * @param name to be set
     */
    public void setName(int name) {
        this.name = name;
    }

    /**
     * Gets the description string for the list item
     * @return the description
     */
    public int getDescription() {
        return description;
    }
}
