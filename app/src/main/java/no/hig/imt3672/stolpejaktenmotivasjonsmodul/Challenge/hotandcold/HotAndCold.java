package no.hig.imt3672.stolpejaktenmotivasjonsmodul.challenge.hotandcold;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.gms.maps.model.LatLng;
import no.hig.imt3672.stolpejaktenmotivasjonsmodul.Poles;
import no.hig.imt3672.stolpejaktenmotivasjonsmodul.R;
import no.hig.imt3672.stolpejaktenmotivasjonsmodul.location.LocationProvider;
import no.hig.imt3672.stolpejaktenmotivasjonsmodul.location.LocationReceiver;

/**
 *
 */
@SuppressWarnings("FieldCanBeLocal")
public class HotAndCold extends Activity implements LocationReceiver {

    private long virtualInitialDistance;
    private long distance;
    private LatLng polePosition;
    private LocationProvider locationProvider;
    private Thermometer thermometer;
    private float percentPoints;

    @SuppressWarnings("ConstantConditions")
    @SuppressLint("AppCompatMethod")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hot_and_cold);

        getActionBar().setTitle(getResources().getString(R.string.hot_and_cold_title));

        thermometer = (Thermometer) findViewById(R.id.thermometer);
        Intent intent = getIntent();
        polePosition = new LatLng(intent.getDoubleExtra("GOAL_LATITUDE", 0),
                                  intent.getDoubleExtra("GOAL_LONGITUDE", 0));
        distance = ((long) intent.getDoubleExtra("DISTANCE", 0));

        percentPoints = 0.0f;
        virtualInitialDistance = ((long) (1.5 * distance));

        try {
            this.locationProvider = LocationProvider.get(this, this);
        } catch (Exception e) {
            Log.w("PROVIDER","UNABLE TO GET PROVIDER:   " + e.getMessage());
        }
    }

    public float getPercentageByDistance() {
        percentPoints =  1.0F - ((float) distance / (float) virtualInitialDistance);

        if (percentPoints > 1.0F) {
            return  (percentPoints - (percentPoints - 1.0F) * 2);
        }

        return (percentPoints < 0.0F)? 0.0F: percentPoints;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_hot_and_cold, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void locationChanged(Location loc) {
        distance = (long) Poles.distance(polePosition, new LatLng(loc.getLatitude(), loc.getLongitude()));
        thermometer.setTemperature(getPercentageByDistance());
    }
}
