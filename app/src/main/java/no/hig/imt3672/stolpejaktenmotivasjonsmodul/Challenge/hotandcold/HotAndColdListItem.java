package no.hig.imt3672.stolpejaktenmotivasjonsmodul.challenge.hotandcold;

import android.content.Context;
import android.content.Intent;
import no.hig.imt3672.stolpejaktenmotivasjonsmodul.R;
import no.hig.imt3672.stolpejaktenmotivasjonsmodul.challenge.ChallengeListItem;

/**
 * The concrete ChallengeListItem for
 * the Hot and cold challenge.
 *
 * Creates the intent necessary to
 * start the Hot And Cold settings activity
 *
 * Sends
 */
public class HotAndColdListItem extends ChallengeListItem {

    public HotAndColdListItem(){
        super(ChallengeListItem.HOT_AND_COLD_ID, R.string.hot_and_cold_name, R.string.hot_and_cold_description);
    }

    @Override
    public Intent createChallengeIntent(Context context) {
        Intent intent;//for "dataflow issues"
        intent = new Intent(context, HotAndColdSettings.class);
        return intent;
    }
}
