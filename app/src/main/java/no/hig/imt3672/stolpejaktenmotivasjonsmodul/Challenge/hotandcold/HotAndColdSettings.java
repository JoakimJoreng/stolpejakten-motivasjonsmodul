package no.hig.imt3672.stolpejaktenmotivasjonsmodul.challenge.hotandcold;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;

import no.hig.imt3672.stolpejaktenmotivasjonsmodul.Pole;
import no.hig.imt3672.stolpejaktenmotivasjonsmodul.Poles;
import no.hig.imt3672.stolpejaktenmotivasjonsmodul.R;
import no.hig.imt3672.stolpejaktenmotivasjonsmodul.location.LocationProvider;

@SuppressWarnings("FieldCanBeLocal")
public class HotAndColdSettings extends Activity implements SeekBar.OnSeekBarChangeListener {
    private final int OFFSET = 500; // compensate for minvalue
    private final int MAXIMUM_RANGE = 3500;
    private final int DEFAULT_RANGE = 1000;

    private int currentRange;
    private TextView difficultyText;
    private CheckBox boxGreen;
    private CheckBox boxBlue;
    private CheckBox boxRed;
    private CheckBox boxBlack;

    private TextView currentRangeText;
    private SeekBar rangeBar;

    private Button startButton;


    private Pole goalPole;

    @SuppressLint("AppCompatMethod")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hot_and_cold_settings);
        Log.i("ACTIVITY: ", "HotAndColdSettings");
        currentRange = DEFAULT_RANGE + OFFSET;
        goalPole = null;

        //noinspection ConstantConditions
        getActionBar().setTitle(getResources().getString(R.string.hot_and_cold_settings_title));

        difficultyText = (TextView) findViewById(R.id.difficultyHeadline);
        boxGreen = (CheckBox) findViewById(R.id.checkBoxGreen);
        boxBlue = (CheckBox) findViewById(R.id.checkBoxBlue);
        boxRed = (CheckBox) findViewById(R.id.checkBoxRed);
        boxBlack = (CheckBox) findViewById(R.id.checkBoxBlack);

        currentRangeText = (TextView) findViewById(R.id.rangeMeterText);
        currentRangeText.setText(currentRange + " meter");

        rangeBar = (SeekBar) findViewById(R.id.rangeBar);
        rangeBar.setMax(MAXIMUM_RANGE);
        rangeBar.setProgress(currentRange);
        rangeBar.setOnSeekBarChangeListener(this);

        startButton = (Button) findViewById(R.id.startButton);

        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startHotAndCold(v);
            }
        });
    }

    /**
     * Starts the actual challenge activity
     * if the settings are verified
     * @param view to be able to start
     * from onClick in layout file
     */
    @SuppressWarnings("UnusedParameters")
    public void startHotAndCold(View view) {
        LatLng location = null;
        try {
            location = LocationProvider.get(this, null).getLastKnownLocation();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (verifyUserInput()) {
            Double distance = Poles.distance(new LatLng(goalPole.getLatitude(), goalPole.getLongitude()),
                    location);
            Intent hotAndColdSettings = new Intent(getApplicationContext(), HotAndCold.class);
            hotAndColdSettings.putExtra("DISTANCE", distance);
            hotAndColdSettings.putExtra("GOAL_POLE_ID", goalPole.getId());
            hotAndColdSettings.putExtra("GOAL_LATITUDE", goalPole.getLatitude());
            hotAndColdSettings.putExtra("GOAL_LONGITUDE", goalPole.getLongitude());

            startActivity(hotAndColdSettings);
        }
    }

    /**
     * Makes sure that the settings are eligible
     * with the surroundings.
     *
     * @return true or false
     */
    private boolean verifyUserInput() {
        boolean[] difficultySettings;

        if (boxGreen.isChecked() || boxBlue.isChecked() ||
                boxRed.isChecked() || boxBlack.isChecked()){
            difficultySettings = new boolean[4];
            difficultySettings[0] = boxGreen.isChecked();
            difficultySettings[1] = boxBlue.isChecked();
            difficultySettings[2] = boxRed.isChecked();
            difficultySettings[3] = boxBlack.isChecked();

            try {
                LocationProvider locationProvider = LocationProvider.get(this, null);
                goalPole = Poles.getInstance().getRandomPoleByParameters(difficultySettings,
                                                                               currentRange,
                                                   locationProvider.getLastKnownLocation());

            } catch (Exception e) {
                e.printStackTrace();
            }

            if (goalPole == null) {
                Toast.makeText(getApplicationContext(), R.string.settings_no_pole_found, Toast.LENGTH_SHORT).show();
                return false;
            }
            Log.i(goalPole.getName(), String.valueOf(goalPole.getDifficulty()));
        }
        else {
            difficultyText.setText(R.string.settings_no_selected_difficulty);
            return false;
        }
        return true;
    }

    /**
     * Displays the description of selected list item
     * in a popup box
     * @param view making onClick
     * from the layoutfile possible
     */
    @SuppressWarnings("UnusedParameters")
    public void showInfo(View view){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.hot_and_cold_difficulty_info);
        builder.setCancelable(true);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        currentRange = progress + OFFSET;
        currentRangeText.setText(currentRange + " meter");
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }
}
