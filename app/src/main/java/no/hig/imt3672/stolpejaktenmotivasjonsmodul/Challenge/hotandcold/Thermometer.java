package no.hig.imt3672.stolpejaktenmotivasjonsmodul.challenge.hotandcold;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Display;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.WindowManager;

import no.hig.imt3672.stolpejaktenmotivasjonsmodul.R;

/**
 * Controlling the Canvas for drawing hot and cold termometer.
 * Based on: http://www.mindfiresolutions.com/Using-Surface-View-for-Android-1659.php#top
 */
@SuppressLint("AndroidLintIconDipSize")
public class Thermometer extends SurfaceView implements SurfaceHolder.Callback {

    //  The percents of the image height showing 0%;
    public static final float HEIGHT_ZERO_PERCENT = 0.28f;

    //  The percents of the image height showing 100%
    public static final float HEIGHT_MAX_PERCENT = 0.90f;

    // Starting temperature"
    public static final float START_VALUE = 0.20f;

    public static final int BOWL_X = 500;
    public static final int BOWL_Y = 1400;

    // For drawing the thermometer at the center.
    private int padding;

    // The height illustrating the current temperature.
    private int heightCurrent;

    // Image(s) height for calculating scaling.
    private int widthScaledImage;

    // The distance from current location to pole in percent.
    private float percent;
    private int virtualHeight;
    private int heightBottom;

    @SuppressWarnings("CanBeFinal")
    private Context context;
    private Bitmap thermometer;
    private Bitmap temperature;
    private ThermometerThread thread;

    public Thermometer(Context context) {
        super(context);
        this.context = context;
        init();
    }

    public Thermometer(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init();
    }

    public Thermometer(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.context = context;
        init();
    }

    /**
     * Initializing the canvas, scaling images to fit screen sizes
     * Calculating position out of percentage.
     */
    private void init() {
        thread = new ThermometerThread(this);
        SurfaceHolder surfaceHolder = getHolder();
        Bitmap sourceThermometerBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.thermometer);
        Bitmap sourceTemperatureBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.temperature);
        percent = START_VALUE;

        // Getting screen size for scaling the image to fit screen
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point point = new Point();
        display.getSize(point);

        // Getting image ratio to calculate new scaled image
        float ratio = (float)sourceThermometerBitmap.getWidth() / (float)sourceThermometerBitmap.getHeight();

        // New sizes, keeping the image ratio
        int height = (int)(point.y * 0.85);
        int width = (int)(height * ratio);
        padding = (point.x - width)/2;

        // Scale image to fit screen
        thermometer = Bitmap.createScaledBitmap(sourceThermometerBitmap, width, height, true);
        temperature = Bitmap.createScaledBitmap(sourceTemperatureBitmap, width, height, true);
        int heightScaledImage = temperature.getHeight();
        widthScaledImage = temperature.getWidth();

        // Get the height to divide in percents.
        heightBottom = temperature.getHeight() - (int)(heightScaledImage * HEIGHT_ZERO_PERCENT);
        int heightTop = temperature.getHeight() - (int)(heightScaledImage * HEIGHT_MAX_PERCENT);
        virtualHeight = heightBottom - heightTop;

        // Amount to change the height. Subtracting = showing more temperature.
        int subtract = (int)(virtualHeight * percent);
        heightCurrent = heightBottom - subtract;
        surfaceHolder.addCallback(this);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        thread = new ThermometerThread(this);
        thread.setRunning(true);
        thread.start();
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
        Log.e("SurfaceChanged", String.valueOf(i) + ", " + i2 + ", " + i3 + ")");
        // TODO animate here?
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        boolean retry = true;

        thread.setRunning(false);
        while(retry) {
            try {
                thread.join();
                retry = false;
            } catch(Exception e) {
                Log.v("Exception Occurred", e.getMessage());
            }
        }
    }

    /**
     * Drawing on canvas, both images and the text showing the temperature
     * @param canvas , canvas being drawn on.
     */
    public void doDraw(Canvas canvas) {
        canvas.drawColor(Color.GRAY);
        Paint paint = new Paint();
        paint.setColor(Color.WHITE);
        paint.setStrokeWidth(20);
        canvas.drawBitmap(temperature, padding, padding, null);
        canvas.drawRect(padding, padding, widthScaledImage + padding, heightCurrent, paint);
        canvas.drawBitmap(thermometer, padding, padding, null);

        Typeface typeface = Typeface.create("Helvetica", Typeface.BOLD);
        Paint fontPaint = new Paint();
        fontPaint.setColor(Color.WHITE);
        fontPaint.setTextSize(100.00f);
        fontPaint.setTypeface(typeface);
        canvas.drawText(String.valueOf(percent) + "C", BOWL_X, BOWL_Y, fontPaint);
    }

    /**
     * Sets the temperature on the thermometer
     * @param percent to be set
     */
    public void setTemperature(float percent) {
        this.percent = (int)(percent * 100);
        heightCurrent = heightBottom - (int)(virtualHeight * percent);
    }
}
