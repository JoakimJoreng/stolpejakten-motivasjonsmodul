package no.hig.imt3672.stolpejaktenmotivasjonsmodul.challenge.hotandcold;

import android.graphics.Canvas;
import android.view.SurfaceHolder;

import no.hig.imt3672.stolpejaktenmotivasjonsmodul.challenge.hotandcold.Thermometer;

/**
 * Handling drawing on canvas with another Thread.
 * Code based on: http://www.mathcs.org/java/android/game_surfaceview.html
 */
public class ThermometerThread extends Thread {

    private boolean running;
    private final SurfaceHolder surfaceHolder;
    @SuppressWarnings("CanBeFinal")
    private Thermometer canvas;

    public ThermometerThread(Thermometer canvas) {
        super();
        this.canvas = canvas;
        this.surfaceHolder = canvas.getHolder();
        running = false;
    }

    public void setRunning(boolean running) {
        this.running = running;
    }

    @Override
    public void run() {
        while(running) {
            Canvas c = canvas.getHolder().lockCanvas();

            if (c != null) {
                synchronized (surfaceHolder) {
                    canvas.doDraw(c);
                }
                surfaceHolder.unlockCanvasAndPost(c);
            }
        }
    }
}
