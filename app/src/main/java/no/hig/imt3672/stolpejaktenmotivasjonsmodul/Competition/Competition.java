package no.hig.imt3672.stolpejaktenmotivasjonsmodul.competition;

import java.util.Calendar;
import java.util.Date;

/**
 * Parent class for all Competitions
 * All competitions have a start and end date.
 */
@SuppressWarnings("FieldCanBeLocal")
public abstract class Competition {

    public static final int DAY = 1;
    public static final int WEEK = 7;
    public static final int MONTH = 30;

    @SuppressWarnings("CanBeFinal")
    private Date startDate;
    @SuppressWarnings("CanBeFinal")
    private Date endDate;
    @SuppressWarnings("CanBeFinal")
    private int days;

    Competition(int days) {
        this.days = days;
        Calendar calendar = Calendar.getInstance();
        Date start = calendar.getTime();
        calendar.add(Calendar.DATE, days);
        Date end = calendar.getTime();

        startDate = start;
        endDate = end;
    }

    public int getDays() {
        return days;
    }
}
