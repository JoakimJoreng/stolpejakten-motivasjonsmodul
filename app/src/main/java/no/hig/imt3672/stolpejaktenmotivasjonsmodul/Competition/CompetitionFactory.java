package no.hig.imt3672.stolpejaktenmotivasjonsmodul.competition;

/**
 * Following the Factory pattern for creating
 * a competition at run time.
 */
public class CompetitionFactory {

    public static final int COMPETITION_STANDARD_DAY = 1;
    public static final int COMPETITION_STANDARD_WEEK = 2;
    public static final int COMPETITION_STANDARD_MONTH = 3;

    public Competition createCompetition(int id) {

        switch(id) {
            case COMPETITION_STANDARD_DAY: return new StandardCompetition(Competition.DAY);
            case COMPETITION_STANDARD_WEEK:  return new StandardCompetition(Competition.WEEK);
            case COMPETITION_STANDARD_MONTH: return new StandardCompetition(Competition.MONTH);
            default: return null;
        }

    }

}
