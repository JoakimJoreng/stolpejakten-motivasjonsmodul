package no.hig.imt3672.stolpejaktenmotivasjonsmodul.competition;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListAdapter;

import java.util.ArrayList;
import java.util.List;

import no.hig.imt3672.stolpejaktenmotivasjonsmodul.R;
import no.hig.imt3672.stolpejaktenmotivasjonsmodul.User;
import no.hig.imt3672.stolpejaktenmotivasjonsmodul.WebResourceHandler;

/**
 * The list showing all available competitions
 */
@SuppressWarnings("EmptyMethod")
public class CompetitionList extends ListActivity {
    private List<CompetitionListItem> items;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.competition_list);

        // Adding 3 competitions:
        items = new ArrayList<CompetitionListItem>();
        items.add(new StandardCompetitionListItem(1, getResources().getString(R.string.competition_standard_day_title), getResources().getString(R.string.competition_standard_week_info)));
        items.add(new StandardCompetitionListItem(2, getResources().getString(R.string.competition_standard_week_title), getResources().getString(R.string.competition_standard_day_info)));
        items.add(new StandardCompetitionListItem(3, getResources().getString(R.string.competition_standard_month_title), getResources().getString(R.string.competition_standard_month_info)));

        setListAdapter(new CompetitionListAdapter());
    }

    @Override
    public ListAdapter getListAdapter() {
        return super.getListAdapter();
    }

    /**
     * The listAdapter for showing list view, and handling list interactions.
     */
    public class CompetitionListAdapter extends ArrayAdapter<CompetitionListItem> {

        public CompetitionListAdapter() {
            super(CompetitionList.this,R.layout.competition_list_item, items);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            if (convertView == null) {
                convertView = getLayoutInflater().inflate(R.layout.competition_list_item, parent, false);
            }

            final CompetitionListItem currentItem = items.get(position);

            final Button startCompetitionButton = (Button) convertView.findViewById(R.id.competition_start);
            startCompetitionButton.setText(currentItem.getName());

            startCompetitionButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Competition competition = new CompetitionFactory().createCompetition(currentItem.getId());
                    WebResourceHandler.getInstance(null).postCompetition(User.getInstance().getId(), competition.getDays());
                    finish();
                }
            });

            ImageButton infoChallengeButton = (ImageButton) convertView.findViewById(R.id.challenge_about);
            infoChallengeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setMessage(currentItem.getDescription());
                    builder.setCancelable(true);
                    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });

                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
            });

            return convertView;
        }


    }

}
