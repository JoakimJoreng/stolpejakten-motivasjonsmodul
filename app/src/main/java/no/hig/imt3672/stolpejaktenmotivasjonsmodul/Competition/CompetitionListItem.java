package no.hig.imt3672.stolpejaktenmotivasjonsmodul.competition;

import android.content.Context;
import android.content.Intent;

/**
 * Holding the content for each list item for competition
 */
@SuppressWarnings("SameReturnValue")
public abstract class CompetitionListItem {

    private int id;
    private String name;
    @SuppressWarnings({"FieldCanBeLocal", "CanBeFinal"})
    private String description;

    public CompetitionListItem(int id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    @SuppressWarnings("UnusedParameters")
    public abstract Intent createCompetitionIntent(Context context);

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription () {
        return this.description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
