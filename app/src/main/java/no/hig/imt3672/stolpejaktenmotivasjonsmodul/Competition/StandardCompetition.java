package no.hig.imt3672.stolpejaktenmotivasjonsmodul.competition;

/**
 * All specific competitions have to extend Competition.
 * The ideal example competition here was to
 * let the user set an start and end date them selves.
 * This changed however to creating three different compeition
 * DAY, WEEK, and MONTH.
 */
public class StandardCompetition extends Competition {

    public StandardCompetition(int days) {
        super(days);
    }

}
