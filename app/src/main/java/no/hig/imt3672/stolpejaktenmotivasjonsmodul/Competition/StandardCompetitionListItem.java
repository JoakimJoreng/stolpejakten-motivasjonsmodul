package no.hig.imt3672.stolpejaktenmotivasjonsmodul.competition;

import android.content.Context;
import android.content.Intent;

/**
 *  Should hold any unique elements for Standard competition.
 *  This example competition only require what is needed, the two dates
 *  and does not contain any other elements.
 */
public class StandardCompetitionListItem extends CompetitionListItem {

    public StandardCompetitionListItem(int id, String name, String description) {
        super(id, name, description);
    }

    @Override
    public Intent createCompetitionIntent(Context context) {
        return null;
    }
}
