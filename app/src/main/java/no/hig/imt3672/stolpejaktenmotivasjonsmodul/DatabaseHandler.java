package no.hig.imt3672.stolpejaktenmotivasjonsmodul;

/**
 * Sets ut the local DB
 */

import android.database.sqlite.SQLiteOpenHelper;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

/**
 * Based upon https://bitbucket.org/gtl-hig/imt3662_sql_notes/
 *
 * Utility class for handling all the management of our DB.
 */
public class DatabaseHandler extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 41;
    private static final String DATABASE_NAME = "Stolpejakten";

    /**
     * Sets up db helper.
     * @param context activity context
     */
    public DatabaseHandler(Context context) {

        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(Poles.POLE_CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + Poles.TABLE_POLE);
        onCreate(db);
    }
}