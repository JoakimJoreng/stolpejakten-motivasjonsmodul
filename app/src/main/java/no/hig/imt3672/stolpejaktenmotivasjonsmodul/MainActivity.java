package no.hig.imt3672.stolpejaktenmotivasjonsmodul;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Toast;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import no.hig.imt3672.stolpejaktenmotivasjonsmodul.challenge.ChallengeList;
import no.hig.imt3672.stolpejaktenmotivasjonsmodul.guild.GuildActivity;
import no.hig.imt3672.stolpejaktenmotivasjonsmodul.guild.ScoresActivity;
import no.hig.imt3672.stolpejaktenmotivasjonsmodul.location.LocationProvider;
import no.hig.imt3672.stolpejaktenmotivasjonsmodul.login.LoginActivity;

/**
 * Serves as a main menu activity and also as a
 * start page for the application when the user is
 * registered as logged in.
 */
@SuppressWarnings({"UnusedParameters", "FieldCanBeLocal"})
public class MainActivity extends Activity implements WebResourceReceiver {
    public final static String KEY_GUILD_ID = "guildId";

    private LocationProvider locationProvider;
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Poles.getInstance().initializePoles(getApplicationContext());

        try {
            this.locationProvider = LocationProvider.get(this, null);
        } catch (Exception e) {
            Log.w("LocationProvider: ", e.toString());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("MAIN", "ON RESUME");
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        int id = preferences.getInt(User.KEY_USER_ID, 0);

        if (id > 0) {
            user = User.getInstance();

            user.setId(id);
            user.setLoggedIn(true);
        } else {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        }

    }

    /**
     * Ilustrationg news button from the actual Stolpejakt APP
     * @param view Button triggering the function
     */
    public void startNews(View view){
        Toast.makeText(getApplicationContext(), getResources().getString(R.string.error_not_available), Toast.LENGTH_SHORT).show();
    }

    /**
     * Ilustrationg social button from the actual Stolpejakt APP
     * @param view Button view that trigger the function
     */
    public void startSocial(View view){
        Toast.makeText(getApplicationContext(), getResources().getString(R.string.error_not_available), Toast.LENGTH_SHORT).show();
    }
    public void startScoreView(View view){
        Intent intent = new Intent(getApplicationContext(), ScoresActivity.class);
        startActivity(intent);
    }

    public void startPoleView(View view) {
        Intent intent = new Intent(getApplicationContext(), PoleViewActivity.class);
        startActivity(intent);
    }

    public void startChallengeListView(View view) {
        Intent intent = new Intent(this, ChallengeList.class);
        startActivity(intent);
    }

    /**
     * Redirect to correct Activity deppending on loggedin status and
     * if the user is registered in a guild.
     * @param view View the button triggering the event.
     */
    public void startGuildPage(View view) {
        WebResourceHandler.getInstance(this).getUserGuildId(user.getId());
    }

    /**
     * Checking if network is available.
     * @return boolean if network is set.
     */
    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null  &&  networkInfo.isConnected();
    }

    @Override
    public void handleResponse(JSONArray array) {
        Intent intent;
        try {
            JSONObject object = array.getJSONObject(0);
            int guildId = object.getInt(KEY_GUILD_ID);

            if (guildId > 0) {
                if (user.isLoggedIn()) {
                    intent = new Intent(this, GuildActivity.class);
                } else {
                    intent = new Intent(this, LoginActivity.class);
                }
            } else {
                intent = new Intent(this, RegisterGuildActivity.class);
            }

            startActivity(intent);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
