package no.hig.imt3672.stolpejaktenmotivasjonsmodul;

/**
 * Class that stores the information of one single pole
 * Created by Joakim on 07.11.2014.
 */
public class Pole {
    private int id;
    private int areaId;
    private String name;
    private double latitude;
    private double longitude;
    private double altitude;
    private int difficulty;
    private String qrCode;

    public Pole() {}

    public Pole(int id, int areaId, String name, double latitude,
                double longitude, double altitude, int difficulty, String qrCode) {
        this.id = id;
        this.areaId = areaId;
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
        this.altitude = altitude;
        this.difficulty = difficulty;
        this.qrCode = qrCode;
    }

    public static int GREEN_DIFFICULTY = 1;
    public static int BLUE_DIFFICULTY = 2;
    public static int RED_DIFFICULTY = 3;
    public static int BLACK_DIFFICULTY = 4;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAreaId() {
        return areaId;
    }

    public void setAreaId(int areaId) {
        this.areaId = areaId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getAltitude() {
        return altitude;
    }

    public void setAltitude(double altitude) {
        this.altitude = altitude;
    }

    public int getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(int difficulty) {
        this.difficulty = difficulty;
    }

    public String getQrCode() {
        return qrCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }
}
