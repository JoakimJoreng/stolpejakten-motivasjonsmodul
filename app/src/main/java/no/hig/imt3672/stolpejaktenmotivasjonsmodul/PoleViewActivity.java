package no.hig.imt3672.stolpejaktenmotivasjonsmodul;


import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class PoleViewActivity extends FragmentActivity {

    private GoogleMap mMap; // Might be null if Google Play services APK is not available.
    private LatLng gjovikLatLng;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pole_view);

        gjovikLatLng = new LatLng(60.79543,10.69163);
        setUpMapIfNeeded();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }

    /**
     * Sets up the map if it is possible to do so (i.e., the Google Play services APK is correctly
     * installed) and the map has not already been instantiated.. This will ensure that we only ever
     * call {@link #setUpMap()} once when {@link #mMap} is not null.
     * <p>
     * If it isn't installed {@link SupportMapFragment} (and
     * {@link com.google.android.gms.maps.MapView MapView}) will show a prompt for the user to
     * install/update the Google Play services APK on their device.
     * <p>
     * A user can return to this FragmentActivity after following the prompt and correctly
     * installing/updating/enabling the Google Play services. Since the FragmentActivity may not
     * have been completely destroyed during this process (it is likely that it would only be
     * stopped or paused), {@link #onCreate(Bundle)} may not be called again so we should call this
     * method in {@link #onResume()} to guarantee that it will be called.
     */
    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                setUpMap();
            }
        }
    }

    /**
     * Renders all the poles to the map as customized markers
     * zooms in on Gjøvik
     */
    private void setUpMap() {
        BitmapDescriptor poleBmp;

        for (Pole pole : Poles.getInstance().getList()) {
            switch (pole.getDifficulty()) {
                case 1: poleBmp = BitmapDescriptorFactory.fromResource(R.drawable.green_pole);
                    break;
                case 2: poleBmp = BitmapDescriptorFactory.fromResource(R.drawable.blue_pole);
                    break;
                case 3: poleBmp = BitmapDescriptorFactory.fromResource(R.drawable.red_pole);
                    break;
                case 4: poleBmp = BitmapDescriptorFactory.fromResource(R.drawable.black_pole);
                    break;
                default: poleBmp = BitmapDescriptorFactory.defaultMarker();
                    break;
            }
            Log.i("POLEDATA", pole.getName() + "   " + pole.getLatitude() + "  " + pole.getLongitude());
            mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(pole.getLatitude(), pole.getLongitude()))
                    .title(pole.getName()))
                    .setIcon(poleBmp);
        }

        CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(gjovikLatLng, 12.0f);
        mMap.animateCamera(yourLocation);
    }
}
