package no.hig.imt3672.stolpejaktenmotivasjonsmodul;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Container class for <Pole>
 * Singleton
 *
 * Contains all the poles in a list which
 * is retrieved from local database or
 * through a call to the stolpejakten API
 * if the local database is empty
 */
public class Poles {
    private ArrayList<Pole> poles;
    private DatabaseHandler dbHandler;
    private Context context;

    private Poles() {
        context = null;
        poles = null;
        dbHandler = null;
    }

    private static class SingletonHolder {
        private static final Poles INSTANCE = new Poles();
    }

    /**
     * Gets the Poles instance
     * @return INSTANCE
     */
    public static Poles getInstance() {
        return SingletonHolder.INSTANCE;
    }

    /**
     * @return the list of poles
     */
    public ArrayList<Pole> getList() {
        return poles;
    }

    /**
     * Initializes the poles list by accessing the local database
     * or the web if the local database had no entries.
     * @param context for the DatabaseHandler
     */
    public void initializePoles(Context context) {
        this.context = context;
        this.poles = new ArrayList<Pole>();
        this.dbHandler = new DatabaseHandler(context);

        SQLiteDatabase db = dbHandler.getWritableDatabase();
        final Cursor c = db.query(TABLE_POLE,
                new String[] { POLE_ID, POLE_AREA_ID, POLE_NAME,
                        POLE_LATITUDE, POLE_LONGITUDE, POLE_ALTITUDE,
                        POLE_DIFFICULTY, POLE_QR_CODE }, null, null, null, null, null);
        // make sure you start from the first item
        c.moveToFirst();
        while (!c.isAfterLast()) {
            final Pole pole = cursorToPole(c);
            poles.add(pole);
            c.moveToNext();
        }
        // Make sure to Fclose the cursor
        c.close();

        if(poles.isEmpty()) {
            new DownloadPoleDataTask().execute(URL_POLES_IN_GJOVIK_1, URL_POLES_IN_GJOVIK_2);
        }
    }

    /**
     * Returns a pole based on parameters
     * @param difficulty presented as a boolean array
     * containing the different difficulties
     * where each arraymember is either enabled or disabled
     * @param maxRange is the maximum distance away from
     * the user location
     * @param playerLocation is the location of the user
     * @return a randomly drawn pole from the selection pool
     */
    public Pole getRandomPoleByParameters(boolean[] difficulty, int maxRange, LatLng playerLocation) {
        final int minRange = 500;
        List<Pole> selectionPool;
        selectionPool = new ArrayList<Pole>();

        for(Pole pole : poles) {
            if (difficulty[pole.getDifficulty()-1]) {
                Double distance = distance(playerLocation, new LatLng(pole.getLatitude(), pole.getLongitude()));
                if (distance < maxRange) {
                    if (distance > minRange) {
                        selectionPool.add(pole);
                    }
                }
            }
        }

        if (!selectionPool.isEmpty()) {
           return selectionPool.get(randomNumber(selectionPool.size()));
        }

        return null;
    }

    /**
     * Calculating distance between two locations
     * Based upon the work of Mr.DavidG
     * http://stackoverflow.com/questions/3694380/
     * calculating-distance-between-two-points-using-latitude-longitude-what-am-i-doi
     * */
    public static double distance(LatLng pos1, LatLng pos2) {

        final int R = 6371; // Radius of the earth

        Double latDistance = deg2rad(pos2.latitude - pos1.latitude);
        Double lonDistance = deg2rad(pos2.longitude - pos1.longitude);
        Double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                + Math.cos(deg2rad(pos1.latitude)) * Math.cos(deg2rad(pos2.latitude))
                * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        double distance;        //for "dataflow issues"
        distance = R * c * 1000;// convert to meters

        return distance;
    }

    private static double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private int randomNumber(int max) {
        int min = 0;
        Random rand = new Random();

        int randomNum;//for "dataflow issues"
        randomNum = rand.nextInt((max - min) + 1) + min;

        return randomNum;
    }

    /**
     * This function shall only be called if the
     * local database is empty
     *
     * Initializes datastructure from JSON and syncs with database
     *
     * @param array JSON ecoded array
     */
    public void initializeInsertFromJSON(JSONArray array) {
        try {
            for(int i = 0; i < array.length(); i++) {

                JSONObject row = array.getJSONObject(i);
                Pole pole = new Pole(row.getInt(POLE_ID), row.getInt(POLE_AREA_ID),
                        row.getString(POLE_NAME), row.getDouble(POLE_LATITUDE),
                        row.getDouble(POLE_LONGITUDE), row.getDouble(POLE_ALTITUDE),
                        row.getInt(POLE_DIFFICULTY), row.getString(POLE_QR_CODE));
                poles.add(pole); // adding to list
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void writePolesToDatabase() {

        SQLiteDatabase db = dbHandler.getWritableDatabase();
        ContentValues values = new ContentValues();

        for(Pole pole : poles) {

            values.put(POLE_ID, pole.getId());
            values.put(POLE_AREA_ID, pole.getAreaId());
            values.put(POLE_NAME, pole.getName());
            values.put(POLE_LATITUDE, pole.getLatitude());
            values.put(POLE_LONGITUDE, pole.getLongitude());
            values.put(POLE_ALTITUDE, pole.getAltitude());
            values.put(POLE_DIFFICULTY, pole.getDifficulty());
            values.put(POLE_QR_CODE, pole.getQrCode());

            db.insert(TABLE_POLE, null, values);
        }
        db.close();
    }

    /**
     * @param c table cursor
     * @return a pole set with data from the database
     */
    public static Pole cursorToPole(Cursor c) {
        final Pole pole = new Pole();

        pole.setId(c.getInt(c.getColumnIndex(POLE_ID)));
        pole.setAreaId(c.getInt(c.getColumnIndex(POLE_AREA_ID)));
        pole.setName(c.getString(c.getColumnIndex(POLE_NAME)));
        pole.setLatitude(c.getDouble(c.getColumnIndex(POLE_LATITUDE)));
        pole.setLongitude(c.getDouble(c.getColumnIndex(POLE_LONGITUDE)));
        pole.setAltitude(c.getDouble(c.getColumnIndex(POLE_ALTITUDE)));
        pole.setDifficulty(c.getInt(c.getColumnIndex(POLE_DIFFICULTY)));
        pole.setQrCode(c.getString(c.getColumnIndex(POLE_QR_CODE)));

        return pole;
    }

    static final String URL_POLES_IN_GJOVIK_1 = "http://www.stolpejakten.no/student/api/poles/7";
    static final String URL_POLES_IN_GJOVIK_2 = "http://www.stolpejakten.no/student/api/poles/1";

    static final String TABLE_POLE = "pole";

    static final String POLE_ID = "pole_id";
    static final String POLE_AREA_ID = "area_id";
    static final String POLE_NAME = "pole_name";
    static final String POLE_LATITUDE = "pole_latitude";
    static final String POLE_LONGITUDE = "pole_longitude";
    static final String POLE_ALTITUDE = "pole_altitude";
    static final String POLE_DIFFICULTY = "pole_difficulty";
    static final String POLE_QR_CODE = "pole_qr_code";

    public static final String POLE_CREATE_TABLE = "CREATE TABLE " + TABLE_POLE
                            + " (" + POLE_ID + " INTEGER PRIMARY KEY, "
                            + POLE_AREA_ID + " INTEGER,"
                            + POLE_NAME + " TEXT,"
                            + POLE_LATITUDE + " DOUBLE,"
                            + POLE_LONGITUDE + " DOUBLE,"
                            + POLE_ALTITUDE + " DOUBLE,"
                            + POLE_DIFFICULTY + " INTEGER,"
                            + POLE_QR_CODE+ " TEXT" +
                            ");";

    public class DownloadPoleDataTask extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... urls) {
            try {
                readJSONFromUrl(urls);
            } catch (Exception e) {
                Log.e("URL:", "Could not retrieve JSON data from stolpejakten" + e.getMessage());

            }
            return null;
        }

        private void readJSONFromUrl(String... urls) throws IOException, JSONException {
            Log.i("URL", String.valueOf(urls.length));
            for (String url : urls) {
                InputStream is = new URL(url).openStream();

                try {
                    BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));

                    StringBuilder sb = new StringBuilder();
                    String line;
                    rd.readLine();
                    while ((line = rd.readLine()) != null) {// Read line by line
                        sb.append(line).append("\n");
                    }

                    JSONArray json = new JSONArray(sb.toString());
                    Poles.getInstance().initializeInsertFromJSON(json);

                } finally {
                    is.close();
                }
            }
            writePolesToDatabase();
        }
    }
}
