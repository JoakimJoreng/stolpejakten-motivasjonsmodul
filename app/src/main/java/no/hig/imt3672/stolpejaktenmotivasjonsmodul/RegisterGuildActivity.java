package no.hig.imt3672.stolpejaktenmotivasjonsmodul;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import no.hig.imt3672.stolpejaktenmotivasjonsmodul.guild.GuildActivity;
import no.hig.imt3672.stolpejaktenmotivasjonsmodul.login.LoginActivity;

/**
 * Handling user registration to a company.
 * This activity gets skiped when user is logged in and already
 * part of a guild.
 */
public class RegisterGuildActivity extends Activity implements WebResourceReceiver {
    private String guildName;
    private EditText codeView;

    @SuppressLint("AppCompatMethod")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_guild);

        //noinspection ConstantConditions
        getActionBar().setTitle(getResources().getString(R.string.register_guild_title));

        guildName = "0";
        codeView =  (EditText) findViewById(R.id.guildCode);
        codeView.setFocusable(true);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_register_guild, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Calles on kode validating before register user to a guild.
     * @param view Button view triggering this event.
     */
    @SuppressWarnings("UnusedParameters")
    public void registerGuild(View view) {
        Intent intent;
        User user = User.getInstance();

        if (user != null  &&  user.isLoggedIn()) {

            String input = codeView.getText().toString();
            WebResourceHandler.getInstance(this).getValidateCode(input, user.getId());
        } else {
            Toast.makeText(getApplicationContext(), R.string.guild_register_code_error, Toast.LENGTH_LONG).show();
            intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        }

    }

    @Override
    public void handleResponse(JSONArray array) {
        Intent intent;

        try {
            JSONObject object = array.getJSONObject(0);
            guildName = object.getString("guildName");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (guildName.equals("0")) {
            codeView.setError(getString(R.string.guild_register_code_feedback));
            codeView.setFocusable(true);
        } else {
            User.getInstance().setGuildName(guildName);
            intent = new Intent(this, GuildActivity.class);
            intent.putExtra("guildName", guildName);
            startActivity(intent);
        }
    }
}
