package no.hig.imt3672.stolpejaktenmotivasjonsmodul;

import android.content.SharedPreferences;

/**
 * User class containing information about the
 * logged in user
 */
public class User {
    public static final String KEY_USER_ID = "USER_ID";

    private int id;
    private boolean loggedIn;
    private String guildName;

    public User() {
        this.id = 0;
        this.loggedIn = false;
        this.guildName = "Guild";

    }

    public void setId(int id) {
        this.id = id;
    }

    public void setGuildName(String guildName){this.guildName = guildName; }

    public String getGuildName() {return guildName; }

    public boolean isLoggedIn() {
        return loggedIn;
    }

    @SuppressWarnings("SameParameterValue")
    public void setLoggedIn(boolean loggedIn) {
        this.loggedIn = loggedIn;
    }

    public int getId() {
        return id;
    }

    public void updateUser(SharedPreferences preferences) {
        id = preferences.getInt(KEY_USER_ID, 0);
        loggedIn = true;
    }

    private static class SingletonHolder {
        private static final User INSTANCE = new User();
    }

    public static User getInstance() {
        return SingletonHolder.INSTANCE;
    }

}


