package no.hig.imt3672.stolpejaktenmotivasjonsmodul;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 *  Handles communication with web resources
 *  using of Async calls to URLS.
 *
 *  All classes making calls with the WebResourceHandler
 *  must implement the WebResourceReceiver interface
 */
public class WebResourceHandler {
    private WebResourceReceiver currentReceiver;
 
    private WebResourceHandler() {}

    /**
     * SingletonHolder is loaded on the first execution of Singleton.getInstance()
     * or the first access to SingletonHolder.INSTANCE, not before.
     */
    private static class SingletonHolder {
        private static final WebResourceHandler INSTANCE = new WebResourceHandler();
        
        private static   void setCurrentReceiver(WebResourceReceiver receiver) {
            INSTANCE.currentReceiver = receiver;
        }
    }

    /**
     * @param receiver is a reference to the class
     * that receives the data from the next async task                
     * @return the singleton instance of WebResourceHandler
     */
    public static WebResourceHandler getInstance(WebResourceReceiver receiver) {
        SingletonHolder.setCurrentReceiver(receiver);
        return SingletonHolder.INSTANCE;
    }

    /**
     * GET request
     * Retrieves the id of the guild which the user is part of
     * @param userId to be sent
     */
    public void getUserGuildId(int userId) {
        String[] data = {"http://www.stud.hig.no/~120383/stolpejaktenbackend/get_guild_id.php" + "?userId=" + userId};
        new WebResourceTask(currentReceiver).execute(data);
    }

    /**
     * GET request
     * Retrieves the userId if the admin is a guildmaster
     * else zero
     * @param userId to be sent
     */
    public void getUserIsAdmin(int userId) {
        String[] data = {"http://www.stud.hig.no/~120383/stolpejaktenbackend/get_user_is_admin.php" + "?userId=" + userId};
        new WebResourceTask(currentReceiver).execute(data);
    }

    /**
     * GET request
     * Validate user input, and user joins guild if valid
     * @param code the code given by the user.
     * @param userId user id entering the code.
     */
    public void getValidateCode(String code, int userId){
        String[] data = {"http://www.stud.hig.no/~120383/stolpejaktenbackend/get_code_validation.php" + "?guildCode=" + code + "&userId=" + userId};
        new WebResourceTask(currentReceiver).execute(data);
    }

    /**
     * GET request
     * Get competition data if guild ha any active.
     * @param userId int userId.
     */
    public void getActiveCompetition(int userId) {
        String[] data = {"http://www.stud.hig.no/~120383/stolpejaktenbackend/get_active_competition.php?userId=" + userId};
        new WebResourceTask(currentReceiver).execute(data);
    }

    /**
     * GET request
     * Retrieves list of all guilds and their
     * score in the season competition
     */
    public void getScoreList() {
        String[] data = {"http://www.stud.hig.no/~120383/stolpejaktenbackend/get_guild_score_list.php"};
        new WebResourceTask(currentReceiver).execute(data);
    }

    /**
     * GET request
     * Maps our system userId with Stolpejakt user Id
     * @param stolpejaktenId int stolpejakt Id
     */
    public void getSystemUserId(int stolpejaktenId, String username) {
        String[] data = {"http://www.stud.hig.no/~120383/stolpejaktenbackend/get_system_user_id.php?stolpejaktenId=" + stolpejaktenId + "&username=" + username,
                         String.valueOf(stolpejaktenId),
                         username
        };
        new WebResourceTask(currentReceiver).execute(data);
    }

    /**
     * POST request
     * Update guild to have an active competition.
     * @param userId The user id (admin) activating an new competition.
     * @param days int number of days the competition is going to last.
     */
    public void postCompetition(int userId, int days) {
        String[] data = {"http://www.stud.hig.no/~120383/stolpejaktenbackend/post_create_competition.php",
                            String.valueOf(userId),
                            String.valueOf(days)
        };
        new WebResourceTask(null).execute(data);
    }

    /**
     * Login user based on user input. Using stolpejakt API
     * @param username username, email address
     * @param password password
     */
    public void login(String username, String password) {
        String[] data = {
                "http://www.stolpejakten.no/student/api/login/",
                username,
                password
        };
        new WebResourceTask(currentReceiver).execute(data);
    }

    private static class WebResourceTask extends AsyncTask<String[], Integer, JSONArray> {
        public final static int URL_INDEX = 0;
        @SuppressWarnings("CanBeFinal")
        private WebResourceReceiver receiver;

        public WebResourceTask(WebResourceReceiver receiver) {
            this.receiver = receiver;
        }

        @Override
        protected JSONArray doInBackground(String[]... data) {
            return postData(data[0]);
        }

        @Override
        protected void onPostExecute(JSONArray jsonArray) {
            super.onPostExecute(jsonArray);
            if (jsonArray != null) {
                receiver.handleResponse(jsonArray);
            }
        }

        public JSONArray postData(String[] data) {
            // Create a new HttpClient and Post Header
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(data[URL_INDEX]);
            Log.i("TASK:", "In function postData");
            try {
                // Add your data if this is a POST req
                if (data.length > 1) {
                    List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

                    String name;
                    for (int i = 1; i < data.length; i++) {
                        Log.i("TASK: ", data[URL_INDEX] + " data: " + data[i]);

                        // Stolpejkat API hack
                        // TODO: Refactor the name generator for post requests
                        if (data[URL_INDEX].equals("http://www.stolpejakten.no/student/api/login/")) {
                            name = (i == 1)? "username": "password";
                        } else {
                            name = "data" + i;
                        }

                        nameValuePairs.add(new BasicNameValuePair(name, data[i]));
                    }
                    httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                }

                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);

                System.out.print(response);
                Log.i("RESPONSE: ", response.toString());
                BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
                StringBuilder builder = new StringBuilder();
                String line;

                // Stolpejakt API skip html meta data:
                if (data[URL_INDEX].equals("http://www.stolpejakten.no/student/api/login/")) {
                    reader.readLine();
                }

                while ((line = reader.readLine()) != null) {
                    builder.append(line);
                    builder.append("\n");
                }

                Log.w("Received data", builder.toString());
                JSONTokener tokener = new JSONTokener(builder.toString());
                return new JSONArray(tokener);

            } catch (ClientProtocolException e) {
                Log.w("EXTERNAL DATABASE: ", e.getMessage());
            } catch (IOException e) {
                Log.w("EXTERNAL DATABASE: ", e.getMessage());
            } catch (JSONException e) {
                Log.w("JSON: ", e.getMessage());
            }

            return null;
        }
    }
}
