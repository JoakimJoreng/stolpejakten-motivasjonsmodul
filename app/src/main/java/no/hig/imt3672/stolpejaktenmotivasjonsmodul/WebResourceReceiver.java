package no.hig.imt3672.stolpejaktenmotivasjonsmodul;

import org.json.JSONArray;

/**
 * Receiver that handles the response
 * from a web resource request
 */
public interface WebResourceReceiver {
    void handleResponse(JSONArray array);
}
