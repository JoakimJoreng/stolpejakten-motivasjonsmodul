package no.hig.imt3672.stolpejaktenmotivasjonsmodul.guild;

import java.util.ArrayList;
import java.util.List;


/**
 * Guild reperesents a company containing at least one
 * team. New joined users gets added to this "default" team.
 */
@SuppressWarnings("FieldCanBeLocal")
public class Guild {

    private String name;
    @SuppressWarnings("CanBeFinal")
    private int adminId;
    @SuppressWarnings({"MismatchedQueryAndUpdateOfCollection", "CanBeFinal"})
    private List<Team> teamList;

    public Guild(String name, UserItem admin) {
        this.name = name;
        this.adminId = 1;
        teamList = new ArrayList<Team>();
        Team team = new Team(name, adminId, admin);
        teamList.add(team);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }



}
