package no.hig.imt3672.stolpejaktenmotivasjonsmodul.guild;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseIntArray;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TabHost;
import android.widget.TextView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import no.hig.imt3672.stolpejaktenmotivasjonsmodul.*;
import no.hig.imt3672.stolpejaktenmotivasjonsmodul.competition.CompetitionList;

/**
 * The company's page showing members and teams created by the admin.
 */
public class GuildActivity extends Activity implements WebResourceReceiver {
    public static final String KEY_IS_ADMIN = "isAdmin";
    public static final String KEY_DATE_START = "startDate";
    public static final String KEY_DATE_END = "endDate";
    public static final String KEY_ACTIVE_COMPETITION = "competitionId";

    private boolean isAdmin;
    private boolean activeCompetition;
    private String activeDateStart;
    private String activeDateEnd;
    private SparseIntArray selectedMembers;

    @SuppressWarnings("ConstantConditions")
    @SuppressLint("AppCompatMethod")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guild);
        String guildName = User.getInstance().getGuildName();

        getActionBar().setTitle(guildName);

        isAdmin = false;
        activeCompetition = false;
        selectedMembers = new SparseIntArray();

        WebResourceHandler.getInstance(this).getUserIsAdmin(User.getInstance().getId());
        WebResourceHandler.getInstance(this).getActiveCompetition(User.getInstance().getId());

        // Setting up Tabs:
        TabHost tabHost = (TabHost) findViewById(R.id.tabHost);
        tabHost.setup();

        TabHost.TabSpec member = tabHost.newTabSpec("Member");
        TabHost.TabSpec team = tabHost.newTabSpec("Team");

        member.setIndicator(getResources().getString(R.string.guild_page_tab_member));
        member.setContent(R.id.fragment_member_list);

        team.setIndicator(getResources().getString(R.string.guild_page_tab_team));
        team.setContent(R.id.fragment_team_list);

        tabHost.addTab(member);
        tabHost.addTab(team);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds nuitems to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_guild, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /**
     * Handling adding members to teams.
     * @param item the button selected
     * @return boolean return success.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        switch (item.getItemId()) {
            case R.id.action_settings:
                Log.e("TEST", "ACTION SETTING");
                return true;
            case R.id.action_confirm:

                if (selectedMembers.size() > 0) {
                    final EditText input = new EditText(this);

                    AlertDialog.Builder alert = new AlertDialog.Builder(this);
                    alert.setTitle(R.string.guild_page_new_team_dialog_title);
                    alert.setView(input);

                    alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            // TODO: create team and update backend
                        }
                    });

                    alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            // TODO: reset highlighting
                        }
                    });

                    alert.show();
                }

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @SuppressWarnings("UnusedParameters")
    public void startCompetitionListView(View view) {
        Intent intent = new Intent(this, CompetitionList.class);
        startActivity(intent);
    }

    /**
     * Updates selected members on each selected Item.
     * @param members array containing selected ids
     */
    public void updateSelectedMembers(SparseIntArray members) {
        selectedMembers = members;
    }

    /**
     * Handel response from backend
     * @param array JSON data resieved from backend.
     */
    @Override
    public void handleResponse(JSONArray array) {
        try {
            if (array.length() > 0) {

                JSONObject object = array.getJSONObject(0);

                // Competition:
                if (object.has(KEY_ACTIVE_COMPETITION)) {
                    activeCompetition = true;
                    activeDateStart = object.getString(KEY_DATE_START);
                    activeDateEnd = object.getString(KEY_DATE_END);

                // isAdmin:
                } else {
                    isAdmin = object.getInt(KEY_IS_ADMIN) > 0;

                    // Setting up admin button:
                    Button startCompetition = (Button) findViewById(R.id.challenge_start_private);

                    if (isAdmin  &&  !activeCompetition) {
                        startCompetition.setVisibility(Button.VISIBLE);
                    } else {
                        startCompetition.setVisibility(Button.GONE);
                    }

                    // Updating information:
                    TextView info = (TextView) findViewById(R.id.guild_info);
                    String msg;
                    if (activeCompetition) {
                        msg = getResources().getString(R.string.guild_page_info_active_competition);
                        msg += ("\n" + activeDateStart + "\n" + activeDateEnd);

                    } else {
                        msg = getResources().getString(R.string.guild_page_info_inactive_competition);
                    }

                    info.setText(msg);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
