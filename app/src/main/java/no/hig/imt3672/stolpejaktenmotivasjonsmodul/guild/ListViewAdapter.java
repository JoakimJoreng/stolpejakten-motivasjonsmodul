package no.hig.imt3672.stolpejaktenmotivasjonsmodul.guild;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import no.hig.imt3672.stolpejaktenmotivasjonsmodul.R;

/**
 * Adapter handling both member and team list. Using view-holder pattern for
 * better scrolling performance.
 * Based on: http://www.perfectapk.com/android-listfragment-tutorial.html
 */
public class ListViewAdapter extends ArrayAdapter<ListItem> {

    @SuppressWarnings("CanBeFinal")
    List<ListItem> items;
    @SuppressWarnings({"FieldCanBeLocal", "CanBeFinal"})

    public ListViewAdapter(Context context, List<ListItem> items) {
        super(context, R.layout.team_list_item, items);
        this.items = items;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        int listLayout = (items.get(0).getClass().equals(UserItem.class))?
                          R.layout.member_list_item:
                          R.layout.team_list_item;


        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(listLayout, parent, false);

            viewHolder = new ViewHolder();
            viewHolder.name = (TextView) convertView.findViewById(R.id.name);
            viewHolder.score = (TextView) convertView.findViewById(R.id.pole);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        ListItem data = getItem(position);

        viewHolder.name.setText(data.getName());
        viewHolder.score.setText(String.valueOf(data.getScore()));

        return convertView;
    }

    private static class ViewHolder {
        TextView name;
        TextView score;
    }

}
