package no.hig.imt3672.stolpejaktenmotivasjonsmodul.guild;

import android.app.ListFragment;
import android.graphics.Color;
import android.os.Bundle;
import android.util.SparseIntArray;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;

import no.hig.imt3672.stolpejaktenmotivasjonsmodul.R;

/**
 * Setting up listFragment for member tab in GuildActivity.
 */
public class MemberListFragment extends ListFragment {
    private SparseIntArray selectedUsers;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        List<ListItem> users = new ArrayList<ListItem>();

        // TODO: Get actual data from backend:
        users.add(new UserItem("2365", 0));
        users.add(new UserItem("123", 3));
        users.add(new UserItem("1923", 5));
        users.add(new UserItem("23", 3));
        users.add(new UserItem("11111", 3));
        users.add(new UserItem("2222", 9));
        users.add(new UserItem("33334", 10));
        users.add(new UserItem("4345", 11));

        setListAdapter(new ListViewAdapter(getActivity(), users));
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getListView().setDividerHeight(1);
    }

    /**
     * Setting up interaction events to handle selection on longpress
     * @param savedState Bundle savedState
     */
    @Override
    public void onActivityCreated(final Bundle savedState) {
        super.onActivityCreated(savedState);
        selectedUsers = new SparseIntArray();

        getListView().setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long l) {
                TextView user = (TextView) view.findViewById(R.id.name);
                TextView poles = (TextView) view.findViewById(R.id.pole);
                int userId = Integer.valueOf(user.getText().toString());
                int color;

                if (selectedUsers.indexOfValue(userId) > -1) {
                    view.setBackgroundColor(getResources().getColor(R.color.guild));
                    color = Color.WHITE;
                    selectedUsers.removeAt(position);
                } else {
                    view.setBackgroundColor(getResources().getColor(R.color.guildPressed));
                    color = Color.BLACK;
                    selectedUsers.put(position, userId);
                }

                user.setTextColor(color);
                poles.setTextColor(color);

                ((GuildActivity)getActivity()).updateSelectedMembers(selectedUsers);
                return true;
            }
        });

    }

}
