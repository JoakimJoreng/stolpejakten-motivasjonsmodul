package no.hig.imt3672.stolpejaktenmotivasjonsmodul.guild;

/**
 * Created by Pål S on 03.12.2014.
 * Stores one high score list item with id, guildname and poles
 * with setters and getters.
 */

@SuppressWarnings("SameParameterValue")
public class ScoreListItem {

    private int id;
    private String guildName;
    private String totalPoles;

    public ScoreListItem(@SuppressWarnings("SameParameterValue") int id, String guildName, String totalPoles) {
        this.id = id;
        this.guildName = guildName;
        this.totalPoles = totalPoles;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getGuildName() {
        return guildName;
    }

    public void setGuildName(String guildName) {
        this.guildName = guildName;
    }

    public String getTotalPoles() {
        return totalPoles;
    }

    public void setTotalPoles(String totalPoles) {
        this.totalPoles = totalPoles;
    }
}
