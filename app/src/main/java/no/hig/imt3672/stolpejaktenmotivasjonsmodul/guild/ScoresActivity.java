
package no.hig.imt3672.stolpejaktenmotivasjonsmodul.guild;
import android.annotation.SuppressLint;
import android.app.ListActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;

import no.hig.imt3672.stolpejaktenmotivasjonsmodul.R;
import no.hig.imt3672.stolpejaktenmotivasjonsmodul.WebResourceHandler;
import no.hig.imt3672.stolpejaktenmotivasjonsmodul.WebResourceReceiver;

/**
 * Activity for displaying the current scoreboard for the guilds.
 * Displays a list showing the number of poles a guild has collected.
 */

@SuppressWarnings("EmptyMethod")
public class ScoresActivity extends ListActivity implements WebResourceReceiver {
    private ArrayList<ScoreListItem> scoreboard;
    private ScoreListAdapter scoreListAdapter;

    @SuppressLint("AppCompatMethod")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scores);

        //noinspection ConstantConditions
        getActionBar().setTitle(getResources().getString(R.string.scores_title));

        WebResourceHandler.getInstance(this).getScoreList();

        scoreboard = new ArrayList<ScoreListItem>();
        scoreListAdapter = new ScoreListAdapter();
        setListAdapter(scoreListAdapter);
    }

    @Override
    public ListAdapter getListAdapter() {
        return super.getListAdapter();
    }

    @Override
    public void handleResponse(JSONArray array) {
        jsonToArraylist(array);
        scoreListAdapter.notifyDataSetChanged();
    }

    private class ScoreListAdapter extends ArrayAdapter<ScoreListItem> {

        public ScoreListAdapter() {

            super(ScoresActivity.this, R.layout.score_list_item, scoreboard);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            if(convertView == null) {
                convertView = getLayoutInflater().inflate(R.layout.score_list_item, parent, false);
            }

            final ScoreListItem currentItem = scoreboard.get(position);

            final TextView scoreItemGuildname = (TextView) convertView.findViewById(R.id.score_item_guildname);
            final TextView scoreItemScore = (TextView) convertView.findViewById(R.id.score_item_score);
            scoreItemGuildname.setText(currentItem.getGuildName());
            scoreItemScore.setText(currentItem.getTotalPoles());

            return convertView;
        }
    }

    /**
     * Method that fills the scoreboard with JSON data downloaded from server
     * Runs after the score is downloaded
     */
    public void jsonToArraylist(JSONArray jsonArray){
        if(jsonArray != null){
            for(int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject;
                try {
                    jsonObject = (JSONObject) jsonArray.get(i);
                    String guildName = jsonObject.getString("guild");
                    String score = jsonObject.getString("score");

                    ScoreListItem listItem = new ScoreListItem(0, guildName, score);
                    scoreboard.add(listItem);
                    Log.d("ITEM ADDED:", listItem.getGuildName() + " " + listItem.getTotalPoles());
                } catch (JSONException e) {
                    Log.e("JSON", "Parsing error");
                }

            }
        }else{
            ScoreListItem listItem = new ScoreListItem(0, getResources().getString(R.string.score_activity_cant_get_score), "");
            scoreboard.add(listItem);
        }
    }


}
