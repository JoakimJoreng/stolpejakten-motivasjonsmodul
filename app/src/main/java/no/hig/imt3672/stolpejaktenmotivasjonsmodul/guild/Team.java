package no.hig.imt3672.stolpejaktenmotivasjonsmodul.guild;

import java.util.ArrayList;
import java.util.List;

/**
 * TeamItem for shoing in list
 */
public class Team extends ListItem {

    private String name;
    private int guildId;
    @SuppressWarnings("CanBeFinal")
    private List<UserItem> userItems;

    /**
     * A Team has to contain at least one player.
     * @param name The name of the Team.
     * @param guildId The id of the guild.
     * @param userItem The first user joining the team.
     */
    public Team(String name, int guildId, UserItem userItem) {
        this.name = name;
        this.guildId = guildId;
        userItems = new ArrayList<UserItem>();
        userItems.add(userItem);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getScore() {
        int score = 0;
        for(UserItem u : userItems) {
            score += u.getScore();
        }
        return score / userItems.size();
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getGuildId() {
        return guildId;
    }

    public void setGuildId(int guildId) {
        this.guildId = guildId;
    }

    public void add(UserItem userItem) {
        userItems.add(userItem);
    }

}
