package no.hig.imt3672.stolpejaktenmotivasjonsmodul.guild;

import android.app.ListFragment;
import android.os.Bundle;
import android.view.View;

import java.util.ArrayList;
import java.util.List;


/**
 * Setting up Team listFragment for displaying int TabHost in guildActivity
 */
public class TeamListFragment extends ListFragment {

    @SuppressWarnings("FieldCanBeLocal")
    private List<ListItem> teams;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // TODO: get actual data from backend
        UserItem joakim = new UserItem("Joakim", 5);
        UserItem paal = new UserItem("Pål", 5);
        UserItem thomas = new UserItem("Thomas", 100);

        Team teamJoakim = new Team("teamJoakim", 1, joakim);
        Team teamPaal = new Team("teamPål", 2, paal);
        Team teamThomas = new Team("teamThomas", 3, thomas);

        teams = new ArrayList<ListItem>();
        teams.add(teamJoakim);
        teams.add(teamPaal);
        teams.add(teamThomas);

        setListAdapter(new ListViewAdapter(getActivity(), teams));
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getListView().setDividerHeight(1);
    }
}
