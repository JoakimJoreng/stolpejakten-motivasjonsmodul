package no.hig.imt3672.stolpejaktenmotivasjonsmodul.guild;

/**
 * UserItem for containing all information to show in each user in
 * member list.
 */
public class UserItem extends ListItem {

    private String name;
    private int score;

    public UserItem(String name, int score) {
        this.name = name;
        this.score = score;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getScore() {
        return score;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setScore(int score) {
        this.score = score;
    }
}
