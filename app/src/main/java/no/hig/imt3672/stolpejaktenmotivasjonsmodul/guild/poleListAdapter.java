package no.hig.imt3672.stolpejaktenmotivasjonsmodul.guild;

/**
 * Interface for using View Holder pattern
 */
public interface poleListAdapter {

    public String getName();
    public int getScore();

}
