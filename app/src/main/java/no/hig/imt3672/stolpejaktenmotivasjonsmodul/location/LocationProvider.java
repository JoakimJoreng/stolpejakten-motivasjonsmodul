package no.hig.imt3672.stolpejaktenmotivasjonsmodul.location;

/**
 * location provider
 *
 * Created by Joakim on 17.11.2014.
 */
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

/**
 * Provides location information.
 */
public class LocationProvider {
    static final int UPDATE_INTERVAL = 1000;
    static final int MINIMUM_DISTANCE = 15;

    private static LocationProvider instance = null;
    @SuppressWarnings("FieldCanBeLocal")
    private LocationManager locationManager;
    private LocationListener locationListener;
    private LatLng location;
    private LocationReceiver listener;

    /**
     * Gets LocationProvider instance
     * @param context The activity context
     * @param listener Who shall receive location updates
     * @return The LocationProvider instance
     */
    public static LocationProvider get(Context context, LocationReceiver listener) throws Exception {
        if (instance == null || listener != instance.listener) {
            instance = new LocationProvider(context, listener);
        }
        return instance;
    }
    private LocationProvider() {}
    private LocationProvider(final Context context, final LocationReceiver listener) throws Exception {
        this.locationManager =  (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        this.listener = listener;
        this.locationListener = new LocationListener() {

            @Override
            public void onLocationChanged(Location loc) {
                Log.i("Location provider", "Got location: " + loc.getLatitude() + "," + loc.getLongitude());
                location = new LatLng(loc.getLatitude(), loc.getLongitude());
                if (listener != null)
                    listener.locationChanged(loc);
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {
                Log.i("STATUSCHANGED: ", provider + " now has status " + status);
            }

            @Override
            public void onProviderEnabled(String provider) {
                /*
                  if(LocationManager.GPS_PROVIDER.equals(provider))
                  If the user has enabled GPS, and no fresh location is registered: enable gps as main provider
                */
            }

            @Override
            public void onProviderDisabled(String provider) {
                // we need the gps to run challenges
            }
        };
        // TODO: Remove all network connections, enable gps only
        if (locationManager.getAllProviders().contains(LocationManager.NETWORK_PROVIDER) &&
                locationManager.isProviderEnabled("network")) {
            Log.i("PROVIDER", "NETWORK IS SET");
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
                    UPDATE_INTERVAL, MINIMUM_DISTANCE, locationListener);

        }
        else if (locationManager.getAllProviders().contains(LocationManager.GPS_PROVIDER) &&
                locationManager.isProviderEnabled("gps")) {
            Log.i("PROVIDER", "GPS IS SET");
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                    UPDATE_INTERVAL, MINIMUM_DISTANCE, locationListener);
        }
        else {
            throw new Exception("No provider available");
        }
    }

    /**
     * Gets the last known location of the user, may be null if nothing has been found.
     * @return The location.
     */
    public LatLng getLastKnownLocation() {
        return location;
    }

    /**
     * Gets the last known latitude.
     * @return Latitude.
     */
    public String getLastKnownLatitude() {
        return String.valueOf(location.latitude);
    }

    /**
     * Gets the last known longitude.
     * @return Longitude.
     */
    public String getLastKnownLongitude() {
        return String.valueOf(location.longitude);
    }

    /**
     * Sets the listener which shall receive location updates.
     * Can be null if we don't want any more updates.
     * @param locationListener The callback object.
     */
    public void setLocationListener(LocationListener locationListener) {
        this.locationListener = locationListener;
    }
}