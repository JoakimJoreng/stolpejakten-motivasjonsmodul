package no.hig.imt3672.stolpejaktenmotivasjonsmodul.location;


import android.location.Location;

/**
 * Implemented in classes that need
 * to receive location updates
 */
public interface LocationReceiver {
    void locationChanged(Location location);
}