package no.hig.imt3672.stolpejaktenmotivasjonsmodul.login;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import no.hig.imt3672.stolpejaktenmotivasjonsmodul.MainActivity;
import no.hig.imt3672.stolpejaktenmotivasjonsmodul.R;
import no.hig.imt3672.stolpejaktenmotivasjonsmodul.User;
import no.hig.imt3672.stolpejaktenmotivasjonsmodul.WebResourceHandler;
import no.hig.imt3672.stolpejaktenmotivasjonsmodul.WebResourceReceiver;

/**
 * A login screen that offers login via email/password.
 * Uses Stolpejakt API.
 */
public class LoginActivity extends Activity implements WebResourceReceiver {

    public final static String KEY_SYSTEM_USER_ID = "userId";
    public final static String KEY_STOKPEJAKT_ID = "ID";

    // UI references.
    private EditText mEmailView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        checkLoggedInUser();

        // Set up the login form.
        mEmailView = (EditText) findViewById(R.id.email);
        mPasswordView = (EditText) findViewById(R.id.password);

        Button mSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);

    }

    /**
     * Checking sharedPreferences of if user
     * is already logged in
     */
    private void checkLoggedInUser() {
        int userId = isLoggedIn();
        if (userId == 0) {
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
            User.getInstance().updateUser(preferences);
        } else {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }
    }

    /**
     * Checks if a user already is logged in.
     * @return the users id, 0 = not logged in.
     */
    public int isLoggedIn() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        return preferences.getInt(User.KEY_USER_ID, 0);
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    public void attemptLogin() {

        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String username = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;


        // Check for a valid password, if the user entered one.
        if (TextUtils.isEmpty(password)) {
            mPasswordView.setError(getString(R.string.error_field_required));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid user name.
        if (TextUtils.isEmpty(username)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!isEmailValid(username)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);

            WebResourceHandler.getInstance(this).login(username, password);
        }
    }

    private boolean isEmailValid(String email) {
        return email.contains("@");
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    public void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    public void handleResponse(JSONArray array) {

        Log.e("LOGIN", "data: " + array.toString());

        int id;
        try {
            JSONObject object = array.getJSONObject(0);

            if (object.has(KEY_STOKPEJAKT_ID)) {
                id = object.getInt(KEY_STOKPEJAKT_ID);
                Log.e("LOGIN", "Stolpejakt id: " + id);

                if (id != 0) {
                    WebResourceHandler.getInstance(this).getSystemUserId(id, mEmailView.getText().toString());
                } else {
                    showProgress(false);
                    mEmailView.setFocusable(false);
                    mPasswordView.setFocusable(true);
                    mPasswordView.setError(getString(R.string.error_invalid_password));
                }

            } else if (object.has(KEY_SYSTEM_USER_ID)) {
                id = object.getInt(KEY_SYSTEM_USER_ID);

                Log.e("LOGIN", "System id: " + id);
                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                SharedPreferences.Editor editor = preferences.edit();
                editor.putInt(User.KEY_USER_ID, id);
                editor.apply();

                User.getInstance().setLoggedIn(true);
                User.getInstance().setId(id);

                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}



